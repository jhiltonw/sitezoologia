<!DOCTYPE html>
<?php
include 'header.php';
include 'left.php';
?>

<div class="container">
    <br/><br/>
    <!--corpo-->
    <center><h1>Publicações</h1></center>

    <div class="border">
        <br/><br/><br/><br/>
        <?php
        require_once 'Controller/PublicacaoControl.php';
        require 'Util/paginacao.php';
        // Número de artigos por página
        $artigos_por_pagina = 5;

        // Página atual
        $pag = filter_input(INPUT_GET, 'pagina', FILTER_VALIDATE_INT);
        $pagina_atual = !empty($pag) ? (int) $pag : 0;
        $pagina_atual = $pagina_atual * $artigos_por_pagina;

        $lista = PublicacaoControl::Listar($pagina_atual, $artigos_por_pagina);

        foreach ($lista as $l) {

            echo '<div class="titulo">
            <p id="negrito">' . $l->getTitulo() . '</p>
        </div>
        
        <div class="data">
            <p id="data">Publicado em: ' . $l->getDate() . '</p>
        </div>';
            if(!empty($l->getImagem1())){
                echo '<center id="ImgPublic"> <img src="./Imgpublicacoes/'.$l->getImagem1().'" width=225px height=225px>';
            }else{
                echo '<center>';
            }
            
            if(!empty($l->getImagem2())){
                echo '<img src="./Imgpublicacoes/'.$l->getImagem2().'" width=225px height=225px>';
            }
            
            if(!empty($l->getImagem3())){
                echo '<img src="./Imgpublicacoes/'.$l->getImagem3().'" width=225px height=225px>';
            }
            
            if(!empty($l->getImagem4())){
                echo '<img src="./Imgpublicacoes/'.$l->getImagem4().'" width=225px height=225px> </center>';
            }else{
                echo '</center>';
            }
            
        echo '<div class="textonoticia">
            <p id="noticia">
                ' . $l->getTexto() . '
            </p>
        </div>
        
        <hr/>
        <br/><br/>';
        }

        $total_artigos = count(PublicacaoControl::ListaTodos());

        // Exibi paginação
        echo '<center>' . paginacao($total_artigos, $artigos_por_pagina, 5) . '</center>';
        ?>
    </div>
</div>


<?php
include 'footer.php';
?>
