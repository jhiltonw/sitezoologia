<?php

/**
 * Description of NewsControl
 *
 * @author Hilton
 */
require_once 'DAO/DAO_News.php';
class NewsControl {
    public static function Listar($inicio, $registros){
        $VLista = DAO_News::Listar($inicio, $registros);
        return $VLista;
    }
    
    public static function ListaTodos() {
        $VLista = DAO_News::ListarTodos();
        return $VLista;
    }
}
