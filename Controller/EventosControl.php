<?php

/**
 * Description of NewsControl
 *
 * @author Hilton
 */
require_once 'DAO/DAO_Eventos.php';
class EventosControl {
    public static function Listar($inicio, $registros){
        $VLista = DAO_Eventos::Listar($inicio, $registros);
        return $VLista;
    }
    
    public static function ListaTodos() {
        $VLista = DAO_Eventos::ListarTodos();
        return $VLista;
    }
}
