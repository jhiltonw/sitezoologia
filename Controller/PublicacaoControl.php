<?php
require_once 'DAO/DAO_Publicacao.php';

/**
 * Description of PublicacaoControl
 *
 * @author Hilton
 */

class PublicacaoControl {
    public static function Listar($inicio, $registros){
        $VLista = DAO_Publicacao::Listar($inicio, $registros);
        return $VLista;
    }
    
    public static function ListaTodos() {
        $VLista = DAO_Publicacao::ListarTodos();
        return $VLista;
    }
}
