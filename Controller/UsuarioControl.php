<?php
require_once 'DAO/DAO_Usuario.php';

/**
 * Description of UsuarioControl
 *
 * @author Hilton
 */
class UsuarioControl {
    
    public static function Autenticar($a, $b){
        $bool = DAO_Usuario::Autenticar($a, $b);
        return $bool;
    }
}
