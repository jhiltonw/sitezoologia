<?php
require_once './cdn/IConfig.php';
require_once 'models/Formulario.php';
require_once './PHPMailer/PHPMailerAutoload.php';
/**
 * Description of Config
 *
 * @author Hilton
 */
class Config implements IConfig{
    
    public function Download($arquivo) {
        
        $bool = false;
        $arquivo_nome = "{$arquivo}";
        $caminho = "./Arquivos/";
        $arq = "{$caminho}/{$arquivo}";
        
        if(!empty($arquivo_nome) && file_exists($arq)){
            header('Content-type: application/zip');
            header("Content-disposition: attachment; filename='{$arquivo_nome}'");
            $bool = readfile($arq);
        }
        return $bool;
    }

    public function EnviarContato($contato) {
        $bool = null;
        $dados = $contato;
        
        $Mailer = new PHPMailer();
        //define o uso de smtp
        $Mailer->IsSMTP();
        
        
        //Aceitar caracteres especiais
        $Mailer->Charset = 'UTF-8';
        //config
        $Mailer->Host = 'smtp.gmail.com';
        $Mailer->Port = 587;
        $Mailer->SMTPAuth = true;
        $Mailer->SMTPSecure = 'tls';
        $Mailer->Username = "cdz.upe.garanhuns@gmail.com";
        $Mailer->Password = "cdzupegaranhuns2018";
        
        //msg config
        $Mailer->setFrom('cdz.upe.garanhuns@gmail.com', 'Site CDZ'); // Definir o remetente da mensagem.
        $Mailer->addAddress('cdz_upe_garanhuns@hotmail.com'); //Definir o destinatário da mensagem.
        $Mailer->Subject = 'Contato via Site';  // O assunto da mensagem.
        //corpo da msg
        $Mailer->Body = '<strong>Mensagem de Contato via Site</strong><br/><br/>
                         <strong>Nome: </strong> '.$dados->getNome().'<br/>
                         <strong>Email: </strong> '.$dados->getEmail().'<br/>
                         <strong>Assunto: </strong> '.$dados->getAssunto().'<br/>
                         <strong>Mensagem: </strong> '.$dados->getMsg().'<br/>';
        
        
        
        
       $Mailer->AltBody = 'Conteudo do email';
       
       If($Mailer->send()){
           $bool = true;
       }else{
           $bool = false;
       }
       return $bool;
        
    }

}
