<?php
include 'header.php';
include 'left.php';
?>
<div class="container">
   
    <!--body-text-->
    <br/><br/>
    <center><h1>O acervo</h1></center>
    <div class="border">
        <br/>
        
        <p class="texto" style="margin-bottom: 11.5%">
            <br/>
            A Coleção Didática de Zoologia (CDZ/UPE) abriga um acervo com mais de 1000 exemplares registrados no livro de tombo, dentre exemplares em álcool, formol e taxidermizados. Espécimes representantes da vasta biodiversidade faunística, principalmente da caatinga e de ambientes costeiros. Atualmente o acervo passa por revisão e aprimoramento. Um recente levantamento resultou um total de 1001 lotes tombados, sendo:
            18 lotes de Poríferos; 
            <br/>
            7 de Cnidários; 
            <br/>
            1 de Platelmintos; 
            <br/>
            2 de Nematelmintos; 
            <br/>
            68 de Moluscos;
            <br/>
            4 de Anelídeos; 
            <br/>
            622 de Artrópodos (sendo 289 Crustáceos, 25 Miriápodos, 131 Hexápodos e 177       Quelícerados);
            <br/>
            9 de Equinodermos; 
            <br/>
            1 de Quetognatos;
            <br/>
            1 de Gnatíferos;
            <br/>
            268 de Cordados [sendo 264 Vertebrados (28 Peixes, 89 Anfíbios, 99 Répteis, 23 Aves e 25 Mamíferos) e 2 Urocordados].
            Acesse a planilha eletrônica com todos os espécimes tombados no acervo da CDZ!
            <br/><br/>
            Atualização do acervo 2018.2 clique <a href="Downloads.php?arq=acervo_2018.2.zip">AQUI</a> para download do arquivo.
            
    </p>


    </div>
</div>
<?php
include 'footer.php';

