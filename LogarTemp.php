<?php

require_once'Controller/UsuarioControl.php';
/**
 * Description of Config
 *
 * @author Hilton
 */
$a = (filter_input(INPUT_POST, 'email1', FILTER_DEFAULT));
$b = (filter_input(INPUT_POST, 'senha1', FILTER_DEFAULT));

if (empty($a) || empty($b)) {
    header('Location: Login.php');
    exit();
} else {

    $bool = UsuarioControl::Autenticar($a, $b);
    if ($bool == true) {
        header('Location: AdmHome.php');
        exit();
    } else {
        echo 'erro';
    }
}