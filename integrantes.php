<!DOCTYPE html>
<?php
include 'header.php';
include 'left.php';
?>

<div class="container">
    <br/><br/>
    <!--corpo-->
    <center><h1>Integrantes</h1></center>

    <div class="border">
        <br/><br/><br/><br/>
        
        <div class="integrante">
            <p id="nome">Nome: Prof.ª Dra. Marina de Sá Leitão Câmara Araújo</p><br/>
       
            <p id="funcao">Função: Curadora e Coordenadora da CDZ/UPE </p><br/>
            
            <p id="funcao">Lattes: <a href="http://lattes.cnpq.br/4315354380176563">http://lattes.cnpq.br/4315354380176563</a></p><br/>
            

        </div>
        <hr/>
        <br/><br/>
        
        <?php
        require_once 'Controller/IntegrantesControl.php';
       
        $lista = IntegrantesControl::ListaTodos();
        
        foreach ($lista as $l) {

            echo '<div class="integrante">
            <p id="nome">Nome: ' . $l->getNome() . '</p><br/>
        
            <p id="curso">Curso: ' . $l->getCurso() . '</p><br/>
       
            <p id="funcao">Função: ' . $l->getFuncao() . ' </p><br/>
            
            <p id="acervo">Acervo: ' . $l->getAcervo() .' </p><br/>
            
            <p id="anoIngresso">Ano de ingresso: ' . $l->getAnoIngresso() . '</p><br/>
        
        </div>
        <hr/>
        <br/><br/>';
        }

       
        ?>
    </div>
</div>


<?php
include 'footer.php';

