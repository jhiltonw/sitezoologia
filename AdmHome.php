<?php
include 'AdmHeader.php';
include 'AdmLeft.php';
?>

<div class="container">
    <br/><br/>
    <!--corpo-->
    <center><h1>Notícias</h1></center>

    <div class="border">
        <br/><br/><br/><br/>
        <?php
        require_once 'Controller/NewsControl.php';
        require 'Util/paginacao.php';
        
        // Número de artigos por página
        $artigos_por_pagina = 5;

        // Página atual
        $pag = filter_input(INPUT_GET, 'pagina', FILTER_VALIDATE_INT );
        $pagina_atual = !empty($pag) ? (int) $pag : 0;
        $pagina_atual = $pagina_atual * $artigos_por_pagina;

        $lista = NewsControl::Listar($pagina_atual,$artigos_por_pagina);
        
        foreach ($lista as $l) {

            echo '<div class="titulo">
            <p id="negrito">' . $l->getTitulo() . '</p>
        </div>
        
        <div class="data">
            <p id="data">Publicado em: ' . $l->getDate() . '</p>
        </div>
        
        <div class="textonoticia">
            <p id="noticia">
                ' . $l->getTexto() . '
            </p>
        </div>
        
        <hr/>
        <br/><br/>';
        }

        $total_artigos = count(NewsControl::ListaTodos());

        // Exibi paginação
        echo '<center>'.paginacao($total_artigos, $artigos_por_pagina, 5).'</center>';

        

        ?>
    </div>
</div>


<?php
include 'AdmFooter.php';
?>
