<?php
require_once 'Controller/ContatoControl.php';
require_once 'models/Formulario.php';
/**
 * Description of Config
 *
 * @author Hilton
 */
$contato = new Formulario();
$contato->setNome(filter_input(INPUT_POST, 'nome', FILTER_DEFAULT));
$contato->setEmail(filter_input(INPUT_POST, 'email', FILTER_DEFAULT));
$contato->setAssunto(filter_input(INPUT_POST, 'assunto', FILTER_DEFAULT));
$contato->setMsg(filter_input(INPUT_POST, 'mensagem', FILTER_DEFAULT));

$bool = ContatoControl::Contato($contato);
if($bool == true){
    header('Location: contato.php');
    exit();
}else{
    echo 'Erro ao enviar tente novamente! <a href="javascript:window.history.go(-1)">Voltar</a>';
}
    