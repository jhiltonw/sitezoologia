<?php
include 'header.php';
include 'left.php';
?>
<div class="container">
   
    <!--body-text-->
    <br/><br/>
    <center><h1>A CDZ</h1></center>
    <div class="border" >
        <br/>
        <p class="texto" style="margin-bottom: 31.5%">

            A Coleção Didática de Zoologia da Universidade de Pernambuco (CDZ/UPE) foi fundada em 2013, 
            desempenhando atividades de ensino, pesquisa e extensão. Está localizada no Laboratório de Zoologia 
            da UPE, Campus Garanhuns e é coordenada pela então curadora Prof.ª Dra. Marina de Sá Leitão Câmara Araújo. 
            Clique em <a href="acervo.php">"Acervo"</a> e conheça nossa Coleção Zoológica!
        </p>
        <br/><br/><br/>
        
    </div>
</div>
<?php
include 'footer.php';
?>
