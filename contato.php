<?php
include 'header.php';
include 'left.php';
?>

<div class="container">
    <br/><br/>
    <!--corpo-->
    <center><h1>Formulário para contato</h1></center>

    <div class="border">
        <br/><br/><br/><br/>
        <form class="formContato" action="Form.php" method="post">
            <label for="nome">Nome: </label><br/>
            <input id="nome" name="nome" type="text" placeholder="Seu nome completo" required><br/>
            <label for="email">Email</label><br/>
            <input id="email" name="email" type="text" placeholder="seuemail@exemplo.com" required><br/>
            <label for="assunto">Assunto:</label><br/>
            <input id="assunto" name="assunto" type="text" required><br/>
            <label for="mensagem">Mensagem:</label><br/>
            <textarea id="mensagem" name="mensagem" required rows="14"> </textarea><br/>
            <input id="enviar" type="submit" value="Enviar">
        </form>
    </div>
</div>


<?php
include 'footer.php';
?>
