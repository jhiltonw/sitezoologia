<?php
include 'header.php';
include 'left.php';
?>
<div class="container">
   
    <!--body-text-->
    <br/><br/>
    <center><h1>Visita Técnica</h1></center>
    <div class="border">
        <br/>
        <p class="texto">
            
            A CDZ é uma ferramenta efetiva no processo de ensino e aprendizagem na área de Zoologia e entre
            outras áreas das Ciências Biológicas. Uma vez que possibilita a observação e o contato direto com 
            cada espécime, torna-se evidente maior aquisição do conhecimento teorizado em sala de aula.<br/>
            A CDZ recebe visitas técnicas de diversas instituições de ensino (IFPE, EREM, Grau Técnico, dentre 
            outras) do agreste pernambucano, e até de outros estados. A visita técnica é acompanhada por monitores 
            e estagiários, durante a visita há uma explanação sobre os principais filos existentes e a exposição 
            de espécimes representantes desses filos. <br/>
            A CDZ também realiza empréstimo de material zoológico para fins didáticos, basta solicitar o empréstimo
            e especificar de quais grupos você deseja o empréstimo de espécimes.
            Para AGEDAMENTO DE VISITA ou EMPRÉSTIMO DE ESPÉCIMES, basta clicar no arquivo abaixo e seguir as 
            recomendações de preenchimento do formulário.
            <br/><br/>
            Agendamento de Visita <br/>
            1.	Clique <a href="Downloads.php?arq=visita.zip">AQUI</a> e baixe o formulário;<br/>
            2.	Preencha todos os campos;<br/>
            3.	Envie o formulário preenchido e um oficio da instituição.<br/>
            <br/>
            Empréstimo de Espécimes <br/>
            1.	Clique <a href="Downloads.php?arq=emprestimo.zip">AQUI</a> e baixe o formulário;<br/>
            2.	Preencha todos os campos;<br/>
            3.	Envie o formulário preenchido.
            
    </p>


    </div>
</div>
<?php
include 'footer.php';
?>
