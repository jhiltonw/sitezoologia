<?php
require_once 'models/Eventos.php';
require_once 'Conexao/Conexao.php';
require_once 'DAO/IDAO.php';
/**
 * Description of DAO_News
 *
 * @author Hilton
 */

class DAO_Eventos implements IDAO{
    
    
    public static function Adicionar($a) {
        
       
    }

    public static function Editar($e) {
        
    }

    public static function Excluir($e) {
        
    }

    public static function Listar($inicio, $registros) {
        $pdo = Conexao::conectar();;
        $listaEvento = $pdo->prepare("SELECT id, titulo, date_format(data,'%d/%m/%Y') data, texto FROM eventos ORDER BY id DESC LIMIT $inicio,$registros");
        $listaEvento->execute();
        
        $lista = $listaEvento->fetchAll(PDO::FETCH_ASSOC);
        $VEvento = array();
        foreach ($lista as $l){
            $VE = new Eventos();
            $VE->setId($l["id"]);
            $VE->setTitulo(utf8_encode($l["titulo"]));
            $VE->setDate($l["data"]);
            $VE->setTexto(utf8_encode($l["texto"]));
            
            $VEvento[] = $VE;
            
        }
        Conexao::desconectar();
        return $VEvento;
    }
    public static function ListarTodos() {
        $pdo = Conexao::conectar();;
        $listaEvento = $pdo->prepare("SELECT id, titulo, date_format(data,'%d/%m/%Y') data, texto FROM eventos ORDER BY id DESC");
        $listaEvento->execute();
        
        $lista = $listaEvento->fetchAll(PDO::FETCH_ASSOC);
        $VEvento = array();
        foreach ($lista as $l){
            $VE = new Eventos();
            $VE->setId($l["id"]);
            $VE->setTitulo(utf8_encode($l["titulo"]));
            $VE->setDate($l["data"]);
            $VE->setTexto(utf8_encode($l["texto"]));
            
            $VEvento[] = $VE;
            
        }
        Conexao::desconectar();
        return $VEvento;
    }
    
}
