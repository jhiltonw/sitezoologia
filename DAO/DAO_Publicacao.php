<?php
require_once 'models/Publicacao.php';
require_once 'Conexao/Conexao.php';
require_once 'DAO/IDAO.php';

/**
 * Description of DAO_Publicacao
 *
 * @author Hilton
 */
class DAO_Publicacao implements IDAO{
    //put your code here
    public static function Adicionar($a) {
        
    }

    public static function Editar($e) {
        
    }

    public static function Excluir($e) {
        
    }

    public static function Listar($inicio, $registros) {
        $pdo = Conexao::conectar();
        $listaPublic = $pdo->prepare("SELECT id, titulo, date_format(data,'%d/%m/%Y') data, texto, imagem1, imagem2, imagem3, imagem4 FROM publicacoes ORDER BY id DESC LIMIT $inicio,$registros");
        $listaPublic->execute();
        
        $lista = $listaPublic->fetchAll(PDO::FETCH_ASSOC);
        $VPublic = array();
        foreach ($lista as $l){
            $VP = new Publicacao();
            $VP->setId($l["id"]);
            $VP->setTitulo(utf8_encode($l["titulo"]));
            $VP->setDate($l["data"]);
            $VP->setTexto(utf8_encode($l["texto"]));
            $VP->setImagem1($l["imagem1"]);
            $VP->setImagem2($l["imagem2"]);
            $VP->setImagem3($l["imagem3"]);
            $VP->setImagem4($l["imagem4"]);
            
            $VPublic[] = $VP;
            
        }
        Conexao::desconectar();
        return $VPublic;
    }

    public static function ListarTodos() {
        $pdo = Conexao::conectar();;
        $listaPublic = $pdo->prepare("SELECT id, titulo, date_format(data,'%d/%m/%Y') data, texto, imagem1, imagem2, imagem3, imagem4 FROM publicacoes ORDER BY id DESC");
        $listaPublic->execute();
        
        $lista = $listaPublic->fetchAll(PDO::FETCH_ASSOC);
        $VPublic = array();
        foreach ($lista as $l){
            $VP = new Publicacao();
            $VP->setId($l["id"]);
            $VP->setTitulo(utf8_encode($l["titulo"]));
            $VP->setDate($l["data"]);
            $VP->setTexto(utf8_encode($l["texto"]));
            $VP->setImagem1($l["imagem1"]);
            $VP->setImagem2($l["imagem2"]);
            $VP->setImagem3($l["imagem3"]);
            $VP->setImagem4($l["imagem4"]);
            
            $VPublic[] = $VP;
            
        }
        Conexao::desconectar();
        return $VPublic;
    }

}
