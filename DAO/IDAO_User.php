<?php

/**
 *
 * @author Hilton
 */
interface IDAO_User {
    
    public static function Adicionar($a);
    public static function Editar($e);
    public static function Excluir($e);
    public static function SelectUser($s);
    public static function Listar($l);
    public static function Autenticar($a, $b);
}
