<?php
//require_once 'Conexao/Conexao.php';
require_once 'models/News.php';
require_once 'Conexao/Conexao.php';
require_once 'DAO/IDAO.php';
/**
 * Description of DAO_News
 *
 * @author Hilton
 */

class DAO_News implements IDAO{
    
    
    public static function Adicionar($a) {
        
       
    }

    public static function Editar($e) {
        
    }

    public static function Excluir($e) {
        
    }

    public static function Listar($inicio, $registros) {
        $pdo = Conexao::conectar();;
        $listaNews = $pdo->prepare("SELECT id, titulo, date_format(data,'%d/%m/%Y') data, texto FROM noticias ORDER BY id DESC LIMIT $inicio,$registros");
        $listaNews->execute();
        
        $lista = $listaNews->fetchAll(PDO::FETCH_ASSOC);
        $VNews = array();
        foreach ($lista as $l){
            $VN = new News();
            $VN->setId($l["id"]);
            $VN->setTitulo(utf8_encode($l["titulo"]));
            $VN->setDate($l["data"]);
            $VN->setTexto(utf8_encode($l["texto"]));
            
            $VNews[] = $VN;
            
        }
        Conexao::desconectar();
        return $VNews;
    }
    public static function ListarTodos() {
        $pdo = Conexao::conectar();;
        $listaNews = $pdo->prepare("SELECT id, titulo, date_format(data,'%d/%m/%Y') data, texto FROM noticias ORDER BY id DESC");
        $listaNews->execute();
        
        $lista = $listaNews->fetchAll(PDO::FETCH_ASSOC);
        $VNews = array();
        foreach ($lista as $l){
            $VN = new News();
            $VN->setId($l["id"]);
            $VN->setTitulo(utf8_encode($l["titulo"]));
            $VN->setDate($l["data"]);
            $VN->setTexto(utf8_encode($l["texto"]));
            
            $VNews[] = $VN;
            
        }
        Conexao::desconectar();
        return $VNews;
    }
    
}
