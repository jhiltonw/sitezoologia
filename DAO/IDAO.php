<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hilton
 */
interface IDAO {
    
    public static function Adicionar($a);
    public static function Editar($e);
    public static function Excluir($e);
    public static function Listar($inicio, $registros);
    public static function ListarTodos();
}
