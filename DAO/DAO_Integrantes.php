<?php
require_once 'models/Integrantes.php';
require_once 'Conexao/Conexao.php';
require_once 'DAO/IDAO.php';

/**
 * Description of DAO_Integrantes
 *
 * @author Hilton
 */
class DAO_Integrantes implements IDAO{
    
    public static function Adicionar($a) {
        
    }

    public static function Editar($e) {
        
    }

    public static function Excluir($e) {
        
    }

   

    public static function ListarTodos() {
        $pdo = Conexao::conectar();;
        $listaIntegrantes = $pdo->prepare("SELECT * FROM integrantes ORDER BY nome ASC");
        $listaIntegrantes->execute();
        
        $lista = $listaIntegrantes->fetchAll(PDO::FETCH_ASSOC);
        $VInte = array();
        foreach ($lista as $l){
            $VI = new Integrantes();
            $VI->setId($l["id"]);
            $VI->setNome(utf8_encode($l["nome"]));
            $VI->setCurso(utf8_encode($l["curso"]));
            $VI->setFuncao(utf8_encode($l["funcao"]));
            $VI->setAcervo(utf8_encode($l["acervo"]));
            $VI->setAnoIngresso($l["anoingresso"]);
            
            $VInte[] = $VI;
            
        }
        Conexao::desconectar();
        return $VInte;
    }
    
    
    //não usar
    public static function Listar($inicio, $registros) {
        
    }

}
