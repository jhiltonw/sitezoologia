<?php

/**
 * Description of Formulario
 *
 * @author Hilton
 */
class Formulario {
    private $nome;
    private $email;
    private $assunto;
    private $msg;
    
    function getNome() {
        return $this->nome;
    }

    function getEmail() {
        return $this->email;
    }

    function getAssunto() {
        return $this->assunto;
    }

    function getMsg() {
        return $this->msg;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setAssunto($assunto) {
        $this->assunto = $assunto;
    }

    function setMsg($msg) {
        $this->msg = $msg;
    }


    
}
