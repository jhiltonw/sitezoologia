<?php

/**
 * Description of Integrantes
 *
 * @author Hilton
 */
class Integrantes {
    private $id;
    private $nome;
    private $curso;
    private $funcao;
    private $acervo;
    private $anoIngresso;
    
    function getId() {
        return $this->id;
    }

    function getNome() {
        return $this->nome;
    }

    function getCurso() {
        return $this->curso;
    }

    function getFuncao() {
        return $this->funcao;
    }

    function getAcervo() {
        return $this->acervo;
    }

    function getAnoIngresso() {
        return $this->anoIngresso;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setCurso($curso) {
        $this->curso = $curso;
    }

    function setFuncao($funcao) {
        $this->funcao = $funcao;
    }

    function setAcervo($acervo) {
        $this->acervo = $acervo;
    }

    function setAnoIngresso($anoIngresso) {
        $this->anoIngresso = $anoIngresso;
    }


}
