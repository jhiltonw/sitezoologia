<?php


/**
 * Description of Publicacao
 *
 * @author Hilton
 */
class Publicacao {
    private $id;
    private $titulo;
    private $date;
    private $texto;
    private $imagem1;
    private $imagem2;
    private $imagem3;
    private $imagem4;
    
    
    function getId() {
        return $this->id;
    }

    function getTitulo() {
        return $this->titulo;
    }

    function getDate() {
        return $this->date;
    }

    function getTexto() {
        return $this->texto;
    }

    function getImagem1() {
        return $this->imagem1;
    }

    function getImagem2() {
        return $this->imagem2;
    }

    function getImagem3() {
        return $this->imagem3;
    }

    function getImagem4() {
        return $this->imagem4;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    function setDate($date) {
        $this->date = $date;
    }

    function setTexto($texto) {
        $this->texto = $texto;
    }

    function setImagem1($imagem1) {
        $this->imagem1 = $imagem1;
    }

    function setImagem2($imagem2) {
        $this->imagem2 = $imagem2;
    }

    function setImagem3($imagem3) {
        $this->imagem3 = $imagem3;
    }

    function setImagem4($imagem4) {
        $this->imagem4 = $imagem4;
    }


    
}
