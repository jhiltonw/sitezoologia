<?php


/**
 * Description of News
 *
 * @author Hilton
 */
class Eventos {
    private $id;
    private $titulo;
    private $date;
    private $texto;
    
    function getId() {
        return $this->id;
    }

    function getTitulo() {
        return $this->titulo;
    }

    function getDate() {
        return $this->date;
    }

    function getTexto() {
        return $this->texto;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    function setDate($date) {
        $this->date = $date;
    }

    function setTexto($texto) {
        $this->texto = $texto;
    }


}
